import static java.nio.charset.StandardCharsets.*;

node("ACE2") {
    dir('C:/Jenkins/workspace/ontrol-test-pipeline_PR-196-M66BUC2F5JXO3FKGGYYA5E4TYRCIUPVV3VBMNYN3WO6VRM3UMJEQ/') {
        def stuff = readFile('new_test_results.xml')
        print(stuff)
        byte[] ptext = stuff.getBytes(ISO_8859_1);
        String value = new String(ptext, UTF_8);
        writeFile(file: 'new_test_results2.xml', text: value)
        junit 'new_test_results2.xml'
    }
}
